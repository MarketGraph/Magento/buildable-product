# Buildable product

Create a new product type in Magento called "buildable product"!
- Which is similar to customizable
- but is hidden from admin Catalog
- And has "Component" parts with dynamic pricing & linked to Inventory!